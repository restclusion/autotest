# autotest

GMAIL: Necesario configurar la "Verificación en dos pasos" y generar una CLAVE 
de uso que permite a la aplicación mandar correos.

PASOS:

	1 - Entre en su cuenta de GMAIL.
	
	2 - En el icono superior derecho de su cuenta, vaya a "Cuenta de Google".
	
	3 - Diríjase a la pestaña de Seguridad (Panel izquierdo).
	
	4 - En "Iniciar sesión en Google", vaya a "Verificación en dos pasos" 
	    y pinche "Empezar".
	    
	5 - Siga los pasos que le proporciona el asistente de Google.
	
	6 - Una vez activada la "Verificación en dos pasos", 
	    repita los pasos del 1 al 3.
	    
	7 - En "Iniciar sesión en Google", vaya a "Contraseñas de aplicaciones".
	
	8 - En "Seleccionar aplicación", elija "Otra" e introduzca el nombre 
	    que considere oportuno.
	    
	9 - Al darle a "Generar", se le mostrará una contraseña ESPECÍFICA 
	    para la aplicación. Esta contraseña será la que use cuando quiera mandar
	    un correo desde la aplicación.
	---------------------------------------------------------------------------