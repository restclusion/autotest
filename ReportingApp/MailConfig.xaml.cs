﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReportingApp
{
    public partial class MailConfig : Window
    {

        private ReportingModel model;
        private MainWindow main;
        public MailConfig()
        {
            model = new ReportingModel();
            InitializeComponent();
        }

        // backToMw: Closes MailConfig window and returns to MainWindow
        private void backToMw(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void setMain(MainWindow themain)
        {
            main = themain;
        }

        //
        private void sendConfToRep(object sender, RoutedEventArgs e)
        {
            main.setMailConf(confMail.Text);

            this.Close();
            //model.setSender(confMail.Text);
            //model.setPwd(confMailPwd.ToString());
        }
    }
}
