﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ReportingApp
{
    public partial class HistoricalView : Window
    {
        //reference to the model
        private ReportingModel model;
        //reference to previous window,the main one
        private MainWindow main;
        public HistoricalView(ReportingModel themodel)
        {
            model = themodel;
            InitializeComponent();
            //load existing categories into the view
            chargeItems();
        }

        //confirmation and back buttons
        private void okButton(object sender, RoutedEventArgs e)
        {
            if (main.IsValidEmail(prevEmail.Text)) {
                main.setMainMail(prevEmail.Text);
                main.Show();
                Close();
            }else {
                FormatErrorMessage.Content = "Email/s wrong format";
            }
        }
        private void backButton(object sender, RoutedEventArgs e)
        {
            main.Show();
            Close();
        }
        //MAIN FUNCTIONS

        //add a new category to the system, user type the name
        private void addCategory(object sender, RoutedEventArgs e)
        {
            bool pass = true;
            if (!string.IsNullOrEmpty(CategoryText.Text))
            {
                 pass=model.addCategoryGeneral(CategoryText.Text);
                FormatErrorMessage.Content = "New category added";
            }
            else {
                FormatErrorMessage.Content = "Add a valid name for the category";
            }
            if (!pass) {
                FormatErrorMessage.Content ="Category name already exist";
            }
            chargeItems();
        }

        //delete a selected category from the directory, user must select the category
        private void deleteCategory(object sender, RoutedEventArgs e) {
            if (EmailDir.SelectedItem!=null &&  !string.IsNullOrEmpty(EmailDir.SelectedItem.ToString())) {
                if (model.removeCategory(EmailDir.SelectedItem.ToString()))
                {
                    FormatErrorMessage.Content = "Deleted " + EmailDir.SelectedItem.ToString() + " category";
                    Subdir.ItemsSource = new List<string>();
                    subdirShow();
                }
                else FormatErrorMessage.Content = "Error deleting the category";
            }
            chargeItems();
        }

        //add an email to a especific category,user select from options list or type the new email
        private void addEmail(object sender, RoutedEventArgs e)
        {
            if (EmailText.Text!=null && !string.IsNullOrEmpty(EmailText.Text) && main.IsValidEmail(EmailText.Text)) {
                if (EmailDir.SelectedItem!=null && !string.IsNullOrEmpty(EmailDir.SelectedItem.ToString()))
                {
                    if (model.addEmail(EmailDir.SelectedItem.ToString(),EmailText.Text))
                    {
                        FormatErrorMessage.Content = EmailText.Text + " added to " + EmailDir.SelectedItem.ToString();
                        refreshSubdir();
                        subdirShow();
                    }
                    else {
                        FormatErrorMessage.Content = "Error adding " +EmailText.Text +" to " + EmailDir.SelectedItem.ToString();
                    }
                }
                else {
                    FormatErrorMessage.Content = "Please select a category in the list to add email";
                }
            }
            else {
                FormatErrorMessage.Content = "Please type a valid email";
            }
        }

        //delete an email from a category, user must select the email inside the category.
        private void deleteEmail(object sender, RoutedEventArgs e) {
            if (Subdir.SelectedItem != null && !string.IsNullOrEmpty(Subdir.SelectedItem.ToString()))
            {
                if (EmailDir.SelectedItem != null && !string.IsNullOrEmpty(EmailDir.SelectedItem.ToString())) {
                   if(model.deleteEmail(EmailDir.SelectedItem.ToString(), Subdir.SelectedItem.ToString())){
                        FormatErrorMessage.Content = "Email deleted succesfully from " + EmailDir.SelectedItem.ToString();
                        refreshSubdir();
                        subdirShow();
                    }else {
                        FormatErrorMessage.Content = "Error deleting email from " + EmailDir.SelectedItem.ToString();
                    }
                }
            }
            else {
                FormatErrorMessage.Content = "Please select an email to delete";
            }
        }

        //add an email to the senders field, email already exists in the category directory
        private void selectEmail(object sender, MouseButtonEventArgs e) {
            if (Subdir.SelectedItem!=null && !string.IsNullOrEmpty(Subdir.SelectedItem.ToString())) {
                if (prevEmail.Text.EndsWith(";") || prevEmail.Text.Length == 0) {
                    prevEmail.Text = prevEmail.Text + Subdir.SelectedItem.ToString() + ";";
                }
                else {
                    prevEmail.Text = ";" + prevEmail.Text + Subdir.SelectedItem.ToString() + ";";
                }
            }
        }

        //remove last added email from the senders field
        private void removeLastEmail(object sender, RoutedEventArgs e) {
            if (prevEmail.Text.Length!=0) {
                string emails = prevEmail.Text;
                if (emails.Contains(";"))
                {
                    emails = emails.TrimEnd(';');
                    emails = emails.Remove(emails.LastIndexOf(';')+1);
                    prevEmail.Text = emails;
                }
                else {
                    prevEmail.Text = "";
                }
            }
        }

        //GRAPHIC DELICATESSENS
        private void wrtEmail(object sender, TextChangedEventArgs e) {
            if (EmailText.Text.Length == 0)
            {
                EmText.Visibility = Visibility.Visible;
            }
            else EmText.Visibility = Visibility.Hidden;
        }
        private void wrtCat(object sender, TextChangedEventArgs e) {
            if (CategoryText.Text.Length == 0)
            {
                CatText.Visibility = Visibility.Visible;
            }
            else CatText.Visibility = Visibility.Hidden;
        }

        private void subdirShow()
        {
            if (Subdir.HasItems)
            {
                Subdir.Visibility = Visibility.Visible;
            }
            else
            {
                Subdir.Visibility = Visibility.Collapsed;
            }
        }

        //AUXILIAR GRAPHIC FUNCTIONS

        //loads subdir elements when a directory is selected
        private void dirLvl1(object sender, SelectionChangedEventArgs e)
        {
            refreshSubdir();
            subdirShow();
        }
   
        //shows the updated elements from the subdirectory
        private void dirLvl2(object sender, RoutedEventArgs e) {
            subdirShow();
        }

        //fill the options list with the options
        private void fillCombo(object sender, EventArgs e)
        {
            string opts = prevEmail.Text;
            string[] optsList = opts.Split(';');
            ObservableCollection<ComboBoxItem> cbItems = new ObservableCollection<ComboBoxItem>();
            EmailOptions.ItemsSource = cbItems;
            foreach (string n in optsList)
            {
                cbItems.Add(new ComboBoxItem { Content = n });
            }
        }

        //fills the addEmail field with the option from the options list.
        private void fillEmail(object sender,EventArgs e)
        {
            if (EmailOptions.SelectedItem!=null) {
                string aux =((ComboBoxItem)EmailOptions.SelectedItem).Content.ToString();
                EmailText.Text =aux;
            }
        }

        //load the directory elements into the view
        private void chargeItems()
        {
            List<string> lista = model.getKeys();
            if (lista!=null) {
                EmailDir.ItemsSource = lista;
            }
        }

        //load the subdir elements on the subdirectory section of the selected directory
        private void refreshSubdir() {
            if (EmailDir.SelectedItem!=null) {
                String element = EmailDir.SelectedItem.ToString();
                List<string> subDirEmails = model.getEmails(element);
                if (subDirEmails != null)
                {
                    Subdir.ItemsSource = subDirEmails;
                }
            }
        }

        //SETTERS
        public void setPrevEmail(string newemail)
        {
            prevEmail.Text = newemail;
        }
        public void setMain(MainWindow lastmain) {
            main = lastmain;
        }
        //OVERRRIDED ONCLOSED METHOD
        protected override void OnClosed(EventArgs e)
        {
            main.Show();
            Close();
        }
    }
}
