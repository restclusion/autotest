﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;

namespace ReportingApp
{
    public partial class MainWindow : Window
    {
        // A ReportingModel model
        private ReportingModel model;
        // The path of the File selected
        private string filedir;

        // MainWindow Constructor
        public MainWindow()
        {
            model = new ReportingModel();
            InitializeComponent();
        }


        // ------------------------- MainWindow METHODS -----------------------------

        // SendToConf: Opens ConfirmationView.
        private void SendToConf(object sender, RoutedEventArgs e)
        {
            if (!IsValidEmail(mailsTextBox.Text) && string.IsNullOrEmpty(mailMessage.Text) && string.IsNullOrEmpty(mailSubject.Text))
            {
                MessageBox.Show("Please introduce an E-Mail/s, a Message or/and a Subject");
            }
            else if (!IsValidEmail(mailsTextBox.Text))
            {
                MessageBox.Show("Not valid E-Mail/s");
            }
            else if (string.IsNullOrEmpty(mailMessage.Text))
            {
                MessageBox.Show("Empty message!");
            }
            else if (string.IsNullOrEmpty(mailSubject.Text))
            {
                MessageBox.Show("Please write the Messages' Subject");
            }
            else
            {
                ConfirmationView confview = new ConfirmationView();
                confview.setMainW(this);
                confview.Show();
            }
        }

        // SendToRep: Sends signal to start ReportingModel processes etc
        public void SendToRep()
        {
            model.setTheMailList(mailsTextBox.Text);
            model.setMailMsg(mailMessage.Text);
            model.setFileDir(filedir);
            model.setMailSubject(mailSubject.Text);
            model.smtpProcess();
        }

        // hintEmail_TextChanged: Hides the Hint Text when the user writes in the Textbox
        private void hintEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (mailsTextBox.Text.Length == 0)
            {
                hintMail.Visibility = Visibility.Visible;
            }
            else hintMail.Visibility = Visibility.Hidden;
        }

        // IsValidEmail: Verifies if the input is a valid email address
        public bool IsValidEmail(string email)
        {
            bool valid = true;
            string[] indvMails = email.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            if (indvMails.Length != 0)
            {
                indvMails[indvMails.Length - 1].Replace(";", "");
            }
            if (indvMails.Length == 0) valid = false;

            foreach (string mail in indvMails)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(mail);
                }
                catch
                {
                    valid = false;
                    break;
                }
            }
            return valid;
        }

        // MailList: Interaction between HistoricalView & MainWindow by the Mail List
        private void MailList(object sender, RoutedEventArgs e)
        {
            HistoricalView histview = new HistoricalView(model);
            if (string.IsNullOrWhiteSpace(mailsTextBox.Text))
            {
                histview.setMain(this);
                histview.Show();
                this.Hide();
            }
            else
            {
                if (IsValidEmail(mailsTextBox.Text) == true)
                {
                    histview.setPrevEmail(mailsTextBox.Text);
                    histview.setMain(this);
                    histview.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Please introduce a valid email: example@mail.com");
                }
            }
        }

        private void MailConfigCl(object sender, RoutedEventArgs e)
        {
            MailConfig emailconf = new MailConfig();
            emailconf.setMain(this);
            emailconf.Show();
        }

        // AddFile: Opens the explorer and lets the user select a file
        private void AddFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Filter for only ".pdf" or "*"
            openFileDialog.Filter = "PDF (*.pdf)|*.pdf|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                filedir = openFileDialog.FileName;
                var labelAux = openFileDialog.FileName;
                string[] splitLabel = labelAux.Split('\\');
                fileLabel.Content = splitLabel[splitLabel.Length - 1];
            }
        }

        // setMainMail: Sets string passed from HistoricalView to mailList
        public void setMainMail(string histMail)
        {
            mailsTextBox.Text = histMail;
        }

        // setMailConf: Sets string passed from MailConfig to mailConfLabel
        public void setMailConf(string passmail)
        {
            mailConfLabel.Content = passmail;
        }
    }
}