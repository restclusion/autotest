﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Net.Mail;
using System.Windows;
using System.Net;
using System.Net.Mime;

namespace ReportingApp
{
    public class ReportingModel
    {
        //static data structure for the saved emails
        private static SortedDictionary<string, ArrayList> RecvsList = new SortedDictionary<string, ArrayList>(new SortListComp());//mirar persistencia de algun tipo
        //body of the mail message
        private string mailMsg;
        //list of email directions to send the report
        private string theMailList;
        //pdf file with the report
        private string file;
        //subject of the mail
        private string mailSubject;
        //sender of the email
        private string mailSender;
        //configuration password for the email
        private string mailPwd;

        //add a new category to the data structure
        public bool addCategoryGeneral(string newCat)
        {
            try
            {
                RecvsList.Add(newCat, new ArrayList());
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        //delete a category from the data structure
        public bool removeCategory(string cat) {
            try
            {
                return RecvsList.Remove(cat);
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        //extracts all the categories from the data structure
        public List<string> getKeys()
        {
            return RecvsList.Keys.ToList();
        }

        //extracts all the stored emails inside a category from the data structure
        public List<string> getEmails(string index)
        {
            ArrayList resultado = new ArrayList();
            if (index != null && RecvsList.ContainsKey(index))
            {
                resultado = RecvsList[index];
            }
            return resultado.Cast<string>().ToList();
        }

        //add an email to a category of the data structure
        public bool addEmail(string cat,string newEmail) {
            bool result = false;
            if (!string.IsNullOrEmpty(cat) && !string.IsNullOrEmpty(newEmail) && RecvsList.ContainsKey(cat)) {
                ArrayList temp = RecvsList[cat];
                temp.Add(newEmail);
                RecvsList[cat] = temp;
                result = true;
            }
            return result;
        }

        //remove an email from a category of the data structure
        public bool deleteEmail(string cat,string badEmail) {
            bool result = false;
            if (!string.IsNullOrEmpty(cat) && !string.IsNullOrEmpty(badEmail) && RecvsList.ContainsKey(cat)) {
                ArrayList temp = RecvsList[cat];
                temp.Remove(badEmail);
                RecvsList[cat] = temp;
                result = true;
            }
                return result;
        }

        //main mail method, send the email with all the fields required
        public void smtpProcess()
        {
            mailSender = "eltestenvio@gmail.com";
            mailPwd = "jnptnesqlkrexraj";
            if (string.IsNullOrEmpty(mailSender) || string.IsNullOrEmpty(mailPwd)) {
                return;
            }

            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//mirar mas emails aparte de gmail

            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.Credentials = new NetworkCredential(mailSender, mailPwd);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(mailSender, "El Test Envio");
            message.From = new MailAddress(mailSender, "Nombre del Remitente");

            string[] theMails = splitMail(theMailList);
            foreach(string mail in theMails)
            {
                message.To.Add(new MailAddress(mail,mail));
            }

            message.Subject = mailSubject;
            message.Body = mailMsg;

            if (file != null)
            {
                Attachment attachment = new Attachment(file, MediaTypeNames.Application.Pdf);
                message.Attachments.Add(attachment);
            }

            message.IsBodyHtml = true;

            smtp.Send(message);

            MessageBox.Show("Mensaje enviado!");
        }

        //auxiliar method to split email string
        private string[] splitMail (string email)
        {
            string[] indvMails = email.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            if (indvMails.Length != 0)
            {
                indvMails[indvMails.Length - 1].Replace(";", "");
            }
            return indvMails;
        }

        // SETTERS
        // setMailMsg: Sets the mail message
        public void setMailMsg(string themsg)
        {
            mailMsg = themsg;
        }

        // setTheMailList: Sets the list of mails
        public void setTheMailList(string maillist)
        {
            theMailList = maillist;
        }

        // setFileDir: Sets the path of the selected file
        public void setFileDir(string filedir)
        {
            file = filedir;
        }

        // setMailSubject: Sets the subject of the mail
        public void setMailSubject(string mailsb)
        {
            mailSubject = mailsb;
        }
        //setter for mail sender
        public void setSender(string origin) {
            mailSender = origin;
        }
        //getter for mailsender
        public string getSender() {
            return mailSender;
        }
        //setter for configuration pwd
        public void setPwd(string configPwd) {
            mailPwd = configPwd;
        }
    }


    //auxiliar comparing class for the data structure to order alphabetically
    public class SortListComp : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return string.Compare(x, y);
        }
    }
}
