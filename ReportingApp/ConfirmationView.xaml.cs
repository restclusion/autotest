﻿using System.Windows;

namespace ReportingApp
{
    public partial class ConfirmationView : Window
    {
        private MainWindow main;
        public ConfirmationView()
        {
            InitializeComponent();
        }

        // backToMain: Closes ConfirmationView
        private void backToMain(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void setMainW (MainWindow themain)
        {
            main = themain;
        }

        // toReportModel: Back to main & activates SendToRep
        private void toReportModel(object sender, RoutedEventArgs e)
        {
            main.SendToRep();
            this.Close();
        }
    }
}
